/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author BOAT
 */
public abstract class Animal {
    private String name;
    private int numberOFLeg;

    public Animal(String name, int numberOFLeg) {
        this.name = name;
        this.numberOFLeg = numberOFLeg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOFLeg() {
        return numberOFLeg;
    }

    public void setNumberOFLeg(int numberOFLeg) {
        this.numberOFLeg = numberOFLeg;
    }

    @Override
    public String toString() {
        return "Animal{" + "name=" + name + ", numberOFLeg=" + numberOFLeg + '}';
    }
    public abstract void eat();
    public abstract void walk();
    public abstract void speak();
    public abstract void sleep();
    
    
}

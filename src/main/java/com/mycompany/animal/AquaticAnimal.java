/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author BOAT
 */
public abstract class AquaticAnimal extends Animal{
    
    public AquaticAnimal(String name) {
        super(name, 0);
        System.out.println("AquaticAnimal");
    }
    
    public abstract void swim();
    
}

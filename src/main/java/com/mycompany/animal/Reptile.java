/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author BOAT
 */
public abstract class Reptile extends Animal{
    
    public Reptile(String name, int numberOFLeg) {
        super(name, numberOFLeg);
        System.out.println("Reptile");
    }
    public abstract void crawl();
}

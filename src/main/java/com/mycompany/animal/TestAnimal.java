/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.animal;

/**
 *
 * @author BOAT
 */
public class TestAnimal {

    public static void main(String[] args) {
        Human human = new Human("Boat");
        human.run();
        human.eat();
        human.walk();
        human.speak();
        human.sleep();
        System.out.println("human is animal ? " + (human instanceof Animal));
        System.out.println("human is land animal ? " + (human instanceof LandAnimal));
        System.out.println("------------");
        Cat cat = new Cat("Leo");
        cat.run();
        cat.eat();
        cat.walk();
        cat.speak();
        cat.sleep();
        System.out.println("cat is animal ? " + (cat instanceof Animal));
        System.out.println("cat is land animal ? " + (cat instanceof LandAnimal));
        System.out.println("------------");
        Dog dog = new Dog("Hunter");
        dog.run();
        dog.eat();
        dog.walk();
        dog.speak();
        dog.sleep();
        System.out.println("dog is animal ? " + (dog instanceof Animal));
        System.out.println("dog is land animal ? " + (dog instanceof LandAnimal));
        System.out.println("------------");
        Fish fish = new Fish("Nimo");
        fish.swim();
        fish.eat();
        fish.speak();
        fish.sleep();
        System.out.println("fish is animal ? " + (fish instanceof Animal));
        System.out.println("fish is aquatic animal ? " + (fish instanceof AquaticAnimal));
        System.out.println("------------");

        Crab crab = new Crab("Nami");
        crab.swim();
        crab.eat();
        crab.speak();
        crab.sleep();
        System.out.println("crab is animal ? " + (crab instanceof Animal));
        System.out.println("crab is aquatic animal ? " + (crab instanceof AquaticAnimal));
        System.out.println("------------");

        Bat bat = new Bat("Covid");
        bat.fly();
        bat.eat();
        bat.walk();
        bat.speak();
        bat.sleep();
        System.out.println("bat is animal ? " + (bat instanceof Animal));
        System.out.println("bat is poultry ? " + (bat instanceof Poultry));
        System.out.println("------------");

        Bird bird = new Bird("Gino");
        bird.fly();
        bird.eat();
        bird.walk();
        bird.speak();
        bird.sleep();
        System.out.println("bird is animal ? " + (bird instanceof Animal));
        System.out.println("bird is poultry ? " + (bird instanceof Poultry));
        System.out.println("------------");

        Crocodile crocodile = new Crocodile("Fedfe");
        crocodile.crawl();
        crocodile.eat();
        crocodile.speak();
        crocodile.sleep();
        System.out.println("crocodile is animal ? " + (crocodile instanceof Animal));
        System.out.println("crocodile is reptile ? " + (crocodile instanceof Reptile));
        System.out.println("------------");

        Snake snake = new Snake("Venom");
        snake.crawl();
        snake.eat();
        snake.speak();
        snake.sleep();
        System.out.println("snake is animal ? " + (snake instanceof Animal));
        System.out.println("snake is reptile ? " + (snake instanceof Reptile));
        System.out.println("------------");

        Animal animal = human;
        System.out.println("animal is animal ? " + (animal instanceof LandAnimal));
        System.out.println("animal is reptile animal ? " + (animal instanceof Reptile));
        System.out.println("animal is reptile animal ? " + (animal instanceof Poultry));
        System.out.println("animal is reptile animal ? " + (animal instanceof AquaticAnimal));
    }
}
